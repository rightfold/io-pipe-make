use v6.c;
use NativeCall;

module IO::Pipe::Make {
    sub native-pipe(CArray[int32] --> int32) is native is symbol('pipe') {*}

    #| C<pipe> creates a pipe and returns a list of two I/O handles that are
    #| the pipe ends. The write-end is the first element of the list.
    sub pipe is export {
        my @ends := CArray[int32].new;
        @ends[0] = @ends[1] = 0;

        my $status = native-pipe(@ends);
        fail 'cannot create pipe' if $status == -1;

        my $w = open("/proc/self/fd/@ends[1]", :w);
        my $r = open("/proc/self/fd/@ends[0]", :r);
        ($w, $r);
    }
}
